% compute the Matlab BEM operator
A=calcSternForComparisonToFFTSVD('../salt.prm','ion.crg','ion.pdb','ion_1.srf','foobar');

% load the FFTSVD BEM operator : note we used salt.prm.
load fftsvdA.txt;
fftsvdA=fftsvdA'; % needs to be transposed because we save it out a
                  % column as a row (multiply by [1; 0 ; 0;], etc)

% this permutation is because the automatic surfaceoperator
% algorithm flips the block order to make complex topologies easy
% to handle algorithmically
srf = loadSternSrfIntoPanels('ion_1.srf');
permuteToMatlabOrder = [2*srf.numSternPanels+1:2*srf.numSternPanels+2*srf.numDielPanels ...
		    1:2*srf.numSternPanels];

Ac = fftsvdA(permuteToMatlabOrder,permuteToMatlabOrder)/4/pi;

clear fftsvdA;
