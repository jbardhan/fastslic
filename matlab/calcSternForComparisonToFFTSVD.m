function A = calcSternForComparisonToFFTSVD(prmFile, crgFile, pdbFile, srfFile, ...
			     outputFile)

loadConstants;
conv_factor = 332.112;

parameters = readPrm(prmFile);
epsIn = parameters.epsIn;
epsOut = parameters.epsOut;
kappa = parameters.kappa;

pqrData = loadPdbAndCrg(pdbFile, crgFile);
srfSternData = loadSternSrfIntoPanels(srfFile);
bemYoonStern = makePanelBemSternMatrices(srfSternData, pqrData, ...
					 epsIn, epsOut, kappa);

A = bemYoonStern.A;

save(outputFile, 'A', '-ascii','-double');
