function parameters = readPrm(prmFile)

fid = fopen(prmFile,'r');

while 1
  line = fgetl(fid);
  if ~ischar(line), break, end
end
fclose(fid);

parameters = struct('epsIn',1,'epsOut',80,'kappa',0.000);