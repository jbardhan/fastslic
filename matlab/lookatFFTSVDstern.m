epsIn = 1;
epsOut = 80;
A=calcSternForComparisonToFFTSVD('../nosalt.prm','ion.crg','ion.pdb','ion_1.srf','foobar');
A = A*4*pi;

srf = loadSternSrfIntoPanels('ion_1.srf');

row1 = 1:srf.numDielPanels;
row2 = srf.numDielPanels+1:2*srf.numDielPanels;
row3 = 2*srf.numDielPanels+1:2*srf.numDielPanels+ ...
       srf.numSternPanels;
row4 = 2*srf.numDielPanels+srf.numSternPanels+1:2* ...
       (srf.numSternPanels+srf.numDielPanels);
col1 = row1;
col2 = row2;
col3 = row3;
col4 = row4;

A11 = A(row1, col1);  A12 = A(row1, col2);  A13 = A(row1, col3);  A14 = A(row1, col4);  
A21 = A(row2, col1);  A22 = A(row2, col2);  A23 = A(row2, col3);  A24 = A(row2, col4);
A31 = A(row3, col1);  A32 = A(row3, col2);  A33 = A(row3, col3);  A34 = A(row3, col4);
A41 = A(row4, col1);  A42 = A(row4, col2);  A43 = A(row4, col3);  A44 = A(row4, col4);

fprintf('%%%%%%%%%%\n')
fprintf('comparing matlab submatrices (A??) to C sub (cA??)\n');
fprintf('%%%%%%%%%%\n')

%%%% row 1 

% A11 should satisfy A11 = cA11
cA11 = load('DielInnerDouble.txt'); cA11 = cA11';
fprintf('norm(A11 - cA11) = %f\n', norm(A11-cA11));

% A12 should satisfy A12 = - cA12
cA12 = load('DielInnerSingle.txt'); cA12 = cA12';
fprintf('norm(A12 - cA12) = %f\n', norm(A12 - (-cA12)));

fprintf('A13, A14 are zero.\n');

%%%% row 2

cdsSingle = load('SternPlusDielSingle.txt'); cdsSingle = cdsSingle';
cdsDouble = load('SternPlusDielDouble.txt'); cdsDouble = cdsDouble';

% A21 should satisfy A21 = 4*pi*I - dtdDouble
cA21=cdsDouble(srf.numSternPanels+1:srf.numSternPanels+srf.numDielPanels,srf.numSternPanels+1:srf.numSternPanels+srf.numDielPanels);
fprintf('norm(A21-(-(cA21-4 pi I))) = %f\n',norm(A21-(-(cA21-4*pi*eye(size(dtdDouble,1))))));

% A22 should satisfy A22 = dtdSingle / epsOut/epsIn
cA22=cdsSingle(srf.numSternPanels+1:srf.numSternPanels+srf.numDielPanels,srf.numSternPanels+1:srf.numSternPanels+srf.numDielPanels);
fprintf('norm(A22-cA22*(epsIn/epsOut)) = %f\n',norm(A22-cA22*(epsIn/epsOut)));

% A23 should satisfy A23 = dtsDouble
cA23=cdsDouble(1+srf.numSternPanels:srf.numSternPanels+srf.numDielPanels,1:srf.numSternPanels);
fprintf('norm(A23-cA23) = %f\n', norm(A23-cA23));

% A24 should satisfy A24 = -dtsSingle
cA24=cdsSingle(1+srf.numSternPanels:srf.numSternPanels+srf.numDielPanels,1:srf.numSternPanels);
fprintf('norm(A24-(-cA24)) = %f\n', norm(A24-(-cA24)));

%%%% row 3

% A31 should satisfy A31 = -stdDouble
cA31=cdsDouble(1:srf.numSternPanels,1+srf.numSternPanels:srf.numSternPanels+srf.numDielPanels);
fprintf('norm(A31-(-cA31)) = %f\n', norm(A31-(-cA31)));

% A32 should satisfy A32 = stdSingle / (epsOut/epsIn)
cA32=cdsSingle(1:srf.numSternPanels,1+srf.numSternPanels:srf.numSternPanels+srf.numDielPanels);
fprintf('norm(A32-cA32*(epsIn/epsOut)) = %f\n',norm(A32 - cA32*(epsIn/epsOut)));

% A33 should satisfy A33 = stsDouble
cA33=cdsDouble(1:srf.numSternPanels,1:srf.numSternPanels);
fprintf('norm(A33-cA33) = %f\n',norm(A33-cA33));

% A34 should satisfy A34 = -cA34
cA34=cdsSingle(1:srf.numSternPanels,1:srf.numSternPanels);
fprintf('norm(A34-cA34) = %f\n', norm(A34-(-cA34)));

%%%% row 4

fprintf('A41, A42 are zero.\n');

% A43 should satisfy A43 = -cA43 + 4 * pi * I
cA43 = load('SternOuterDouble.txt'); cA43 = cA43';
fprintf('norm(A43 - cA43) = %f\n', norm(A43-(-(cA43-4*pi*eye(size(cA43,1))))));

% A44 should satisfy A44 = csoSingle...
cA44 = load('SternOuterSingle.txt'); cA44 = cA44';
fprintf('norm(A44 - cA44) = %f\n', norm(A44-cA44));

