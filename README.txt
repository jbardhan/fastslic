1. To mesh surfaces of your own you will need to download a binary of
MSMS.  This can be obtained from
http://mgltools.scripps.edu/packages/MSMS and then you need to set the
environment variable MSMS_PATH to the location of the binary.

2. You will need SuperLU version 5.0 or later (there is an API change
at 5.0).

3. You will need FFTW version 3.0 or later.

** NOTE: the following does not work yet. I am writing the files to get it to work **

4. To check the standard PB solver's matrix against Matlab's, compile

writeFFTSVDmatrices

and then in any subdirectory of data/ , say data/arg/ , run

writeFFTSVDmatrices ../salt.prm ../scaled_charmm22.siz <crgfile> <pdbfile> <whatever_1.srf> <outputfile>

where the last arguments reference the files in your directory,
e.g. arg.crg, arg.pdb , and arg_1.srf , and outputfile is the filename
you'll load in matlab.


Then run Matlab, add the path to the matlab scripts
(e.g. '~/repos/fastslic/matlab') and from the example directory
(e.g. data/arg) run

compareToFFTSVD(<prmfile>,<crgfile>,<pdbfile>,<srffile>,<newoutputfile>)

Then you can compare the two output files.

5. Need to explain how to do the same thing for the SLIC model.
