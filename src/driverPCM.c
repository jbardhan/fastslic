#include "FFTSVDpbeAPI.h"
#include "PCMproblem.h"
#include "FFTSVD.h"

SIZentry* SIZentries; 
unsigned int numSIZentries;
CRGentry* CRGentries;
unsigned int numCRGentries;
char variablechain = 'V';
char fixedligandchain = 'L';
char fixedreceptorchain = 'R';
unsigned int num_GMRES_iter;
int saveGMRES = 0;
real tol;

int main(int argc, char* argv[]) {
   FILE* vertfile = NULL;
   FILE* facefile = NULL;
   FILE* chargefile = NULL;
   VertFace vertface_d, vertface_s;
   Panel* panels_d;
   Panel* panels_s;
   Panel* totalpanels;
   unsigned int numpanels_d;
   unsigned int numpanels_s;
   unsigned int totalnumpanels;
   Vector3D* centroids_d;
   Vector3D* centroids_s;
   Vector3D* totalcentroids;
   unsigned int i, c;
   Vector rhs;
   Vector sol;
   SurfaceOperator sopi, sopo, soho;
   Vector phi;
   real energy = 0.0;
   if (argc != 9) {
      printf("Usage: %s <param> <siz> <boundCRG> <boundPDB> <diel.vert> <diel.face> <stern.vert> <stern.face>\n", argv[0]);
      return -1;
   }

   printf("Reading parameters from %s\n", argv[1]);
   readParams(argv[1]);
   printf("Checking for valid parameters\n");
   checkParams();
   printf("Reading radii from %s\n", argv[2]);
   readSIZ(argv[2], &numSIZentries, &SIZentries);
   printf("Reading charges from %s\n", argv[3]);
   readCRG(argv[3], &numCRGentries, &CRGentries);

   vertface_d = VertFace_allocate();

   vertfile = fopen(argv[5], "r");

   if (vertfile == NULL) {
      perror("Error opening vertices file");
      return -2;
   }

   VertFace_readvert(vertface_d, vertfile);

   fclose(vertfile);

   facefile = fopen(argv[6], "r");
    
   if (facefile == NULL) {
      perror("Error opening face file");
      return -2;
   }
    
   VertFace_readface_flip(vertface_d, facefile);
    
   fclose(facefile);

   vertface_s = VertFace_allocate();

   vertfile = fopen(argv[7], "r");

   if (vertfile == NULL) {
      perror("Error opening vertices file");
      return -2;
   }

   VertFace_readvert(vertface_s, vertfile);

   fclose(vertfile);

   facefile = fopen(argv[8], "r");
    
   if (facefile == NULL) {
      perror("Error opening face file");
      return -2;
   }
    
   VertFace_readface_flip(vertface_s, facefile);
    
   fclose(facefile);


   VertFace_fix(vertface_d, 0);
   VertFace_fix(vertface_s, 1);

   numpanels_d = vertface_d->numfaces;
   numpanels_s = vertface_s->numfaces;

   totalnumpanels = numpanels_d + numpanels_s;

   panels_d = (Panel*)calloc(numpanels_d, sizeof(Panel));
   panels_s = (Panel*)calloc(numpanels_s, sizeof(Panel));
   totalpanels = (Panel*)calloc(totalnumpanels, sizeof(Panel));

   printf("Constructing and allocating panels... ");
   fflush(stdout);

   VertFace_getpanels(vertface_d, panels_d);
   VertFace_getpanels(vertface_s, panels_s);

   for (i = 0; i < numpanels_s; i++)
      totalpanels[i] = panels_s[i];
   for (i = 0; i < numpanels_d; i++)
      totalpanels[i+numpanels_s] = panels_d[i];

   printf("done.\n");

   centroids_d = (Vector3D*)calloc(numpanels_d, sizeof(Vector3D));
   centroids_s = (Vector3D*)calloc(numpanels_s, sizeof(Vector3D));
   totalcentroids = (Vector3D*)calloc(totalnumpanels, sizeof(Vector3D));
   
   for (i = 0; i < numpanels_s; i++) {
      centroids_s[i] = Vector3D_allocate();
      Vector3D_copy(centroids_s[i], panels_s[i]->centroid);
      totalcentroids[i] = centroids_s[i];
   }
   for (i = 0; i < numpanels_d; i++) {
      centroids_d[i] = Vector3D_allocate();
      Vector3D_copy(centroids_d[i], panels_d[i]->centroid);
      totalcentroids[i+numpanels_s] = centroids_d[i];
   }

   rhs = Vector_allocate(2 * totalnumpanels);
   sol = Vector_allocate(2 * totalnumpanels);

   printf("Constructing and allocating surface operator... ");
   fflush(stdout);

   real kappa = sqrt(0.145) / 3.047;

   soho = SurfaceOperator_allocate();

   soho->kernel = POISSON_KERNEL;
   soho->epsilon = outerdielectric;
   soho->kappa = 0; //kappa;
   soho->mynumpanels = 0;
   soho->tree = Tree_allocate(panels_s, numpanels_s, centroids_s, numpanels_s, MAX_PANELS_PER_FINEST_CUBE, POISSON_KERNEL, NULL, GRID_SIZE, SVD_ERROR, SINGLE_AND_DOUBLE_LAYER_INT, 0.0);
   soho->numchildren = 0;
   soho->children = NULL;
   soho->resultInternal = Vector_allocate(numpanels_s);
   soho->resultExternal = Vector_allocate(numpanels_s);
   soho->parent = NULL;
   soho->charges = NULL;


   sopo = SurfaceOperator_allocate();

   sopo->kernel = POISSON_KERNEL;
   sopo->epsilon = outerdielectric;
   sopo->kappa = 0.0;
   sopo->mynumpanels = numpanels_s;
   sopo->tree = Tree_allocate(totalpanels, totalnumpanels, totalcentroids, totalnumpanels, MAX_PANELS_PER_FINEST_CUBE, POISSON_KERNEL, NULL, GRID_SIZE, SVD_ERROR, SINGLE_AND_DOUBLE_LAYER_INT, 0.0);
   sopo->numchildren = 0;
   sopo->children = NULL;
   sopo->resultInternal = Vector_allocate(totalnumpanels);
   sopo->resultExternal = Vector_allocate(totalnumpanels);
   sopo->parent = NULL;
   sopo->charges = NULL;

   sopi = SurfaceOperator_allocate();

   sopi->kernel = POISSON_KERNEL;
   sopi->epsilon = innerdielectric;
   sopi->kappa = 0.0;
   sopi->mynumpanels = numpanels_d;
   sopi->tree = Tree_allocate(panels_d, numpanels_d, centroids_d, numpanels_d, MAX_PANELS_PER_FINEST_CUBE, POISSON_KERNEL, NULL, GRID_SIZE, SVD_ERROR, SINGLE_AND_DOUBLE_LAYER_INT, 0.0);
   sopi->numchildren = 0;
   sopi->children = NULL;
   sopi->resultInternal = Vector_allocate(numpanels_d);
   sopi->resultExternal = Vector_allocate(numpanels_d);
   sopi->parent = NULL;
   sopi->charges = NULL; // FOR NOW. LATER, charge 


   unsigned int index_soho = 0;
   unsigned int index_sopo = 0;
   unsigned int index_sopi = 0;

   SurfaceOperator_initStartIndices(soho, &index_soho);
   SurfaceOperator_initStartIndices(sopo, &index_sopo);
   SurfaceOperator_initStartIndices(sopi, &index_sopi);
      
   printf("done.\n");

   printf("Determining local and interacting lists... ");
   fflush(stdout);

   Tree_lists(soho->tree);
   Tree_lists(sopo->tree);
   Tree_lists(sopi->tree);

   printf("done.\n");

   printf("Filling tree structure... ");
   fflush(stdout);

   Tree_fill(soho->tree);
   Tree_fill(sopo->tree);
   Tree_fill(sopi->tree);

   printf("done.\n");

   Tree_memory(soho->tree);
   Tree_memory(sopo->tree);
   Tree_memory(sopi->tree);

   PCMproblem problem = PCMproblem_allocate_fromSurfOper(soho, sopo, sopi);
   printf("Reading PDB from %s\n", argv[4]);
   readPDB(argv[4], &(problem->numpdbentries), &(problem->pdbentries));
   assignRadiiCharges(problem->pdbentries, problem->numpdbentries, SIZentries, numSIZentries,
                      CRGentries, numCRGentries);
   
   PCMproblem_writematlabfile(problem, "cA.txt");
   Vector phiSurf = Vector_allocate(problem->numtotalsurfacevariables);
   Vector_zero(phiSurf, problem->numtotalsurfacevariables);
   phiSurf[0] = 1.0;
   PCMproblem_solveSym(problem, phiSurf);
   Vector_writefile("solution.txt", problem->Sol, problem->numtotalsurfacevariables);

   PCMproblem_free(problem);


   SurfaceOperator_free(soho);
   SurfaceOperator_free(sopo);
   SurfaceOperator_free(sopi);
   Vector_free(sol);
   Vector_free(rhs);

   for (i = 0; i < numpanels_d; i++)
      Vector3D_free(centroids_d[i]);
   for (i = 0; i < numpanels_s; i++)
      Vector3D_free(centroids_s[i]);
   free(centroids_d);
   free(centroids_s);
   free(totalcentroids);
   for (i = 0; i < numpanels_d; i++)
      Panel_free(panels_d[i]);
   for (i = 0; i < numpanels_s; i++)
      Panel_free(panels_s[i]);
   free(panels_d);
   free(panels_s);
   free(totalpanels);

   VertFace_free(vertface_d);
   VertFace_free(vertface_s);

   return 0;
}
