CC = gcc
#CFLAGS =  -g -O3 -mtune=pentium4 -mmmx -msse -msse2 -mfpmath=sse,387
CFLAGS = -g 
INCLUDE = -I$(SUPERLU_INC) -I$(FFTW_INC)
FFTW_INC = /usr/local/Cellar/fftw/3.3.5/include
FFTW_LIB = /usr/local/Cellar/fftw/3.3.5/lib
FLAGS = -DMVTIME
LDFLAGS = $(CFLAGS)
LIBDIR =
SUPERLU_INC = /usr/local/Cellar/superlu/5.2.1_1/include/superlu
SUPERLU_LIB = /usr/local/Cellar/superlu/5.2.1_1/lib
LIBS = -L$(SUPERLU_LIB) -L$(FFTW_LIB) -lsuperlu -llapack -lblas -lfftw3 -lfftw3f
