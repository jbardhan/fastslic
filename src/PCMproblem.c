#include "PCMproblem.h"
#include "Integration.h"
#include <sys/time.h>
#include <sys/resource.h>

PCMproblem PCMproblem_allocate_fromSurfOper(SurfaceOperator stern, SurfaceOperator both, SurfaceOperator diel) {
  PCMproblem problem = NULL;
  problem = (PCMproblem)(calloc(1, sizeof(_PCMproblem)));

  problem->stern = stern;
  problem->sternPlusDiel = both;
  problem->diel = diel;

  problem->useasymmetric = 0;
  problem->localE = NULL;
  problem->capacitance_preconditioner = NULL;
  
  /* */
  problem->numsalts = 1;
  problem->saltpanels = (Panel **)calloc(1, sizeof(Panel*));
  problem->saltpanels[0] = stern->tree->panels;
  problem->numsaltpanels = (unsigned int *)calloc(1, sizeof(unsigned int));
  problem->numsaltpanels[0] = stern->tree->numpanels;

  problem->numdielectrics = 1;
  problem->dielectricpanels = (Panel **)calloc(1, sizeof(Panel*));
  problem->dielectricpanels[0] = diel->tree->panels;
  problem->numdielectricpanels = (unsigned int*)calloc(1, sizeof(unsigned int));
  problem->numdielectricpanels[0] = diel->tree->numpanels;
  
  problem->numtotalpanels = problem->numsaltpanels[0] + problem->numdielectricpanels[0];
  problem->numtotalsurfacevariables = 2 * problem->numtotalpanels;

  /* */
  
  problem->preconditioner = NULL;

  problem->numpdbentries = 0;
  problem->pdbentries = NULL;

  problem->numvariablecharges = 0;
  problem->variablechargeindextoglobalindex = NULL;
  problem->numfixedligandcharges = 0;
  problem->fixedligandchargeindextoglobalindex = NULL;
  problem->numfixedreceptorcharges = 0;
  problem->fixedreceptorchargeindextoglobalindex = NULL;

  problem->globalCharges = NULL;
  problem->RHS = Vector_allocate(problem->numtotalsurfacevariables);
  problem->Sol = Vector_allocate(problem->numtotalsurfacevariables);
  problem->surfaceCharges = Vector_allocate(problem->numdielectricpanels[0]);
  problem->globalPhiReact = NULL;
  problem->variablePhiReact = NULL;
  
  return problem;
}

void PCMproblem_free(PCMproblem problem) {

  // uncomment when you initialize PCMproblem directly from panel info

  /* if (problem->stern) */
  /*   SurfaceOperator_free(problem->stern); */
  /* if (problem->sternPlusDiel) */
  /*   SurfaceOperator_free(problem->sternPlusDiel); */
  /* if (problem->diel) */
  /*   SurfaceOperator_free(problem->diel); */
  
  if (problem->preconditioner)
    Preconditioner_free(problem->preconditioner);

  // what about freeing panels, PDBentries?
  if (problem->variablechargeindextoglobalindex)
    free(problem->variablechargeindextoglobalindex);
  
  if (problem->globalCharges) 
    Vector_free(problem->globalCharges);

  if (problem->surfaceCharges) 
    Vector_free(problem->surfaceCharges);

  if (problem->RHS)
    Vector_free(problem->RHS);

  if (problem->RHS)
    Vector_free(problem->Sol);

  if (problem->globalPhiReact) 
    Vector_free(problem->globalPhiReact);

  if (problem->variablePhiReact)
    Vector_free(problem->variablePhiReact);

  if (problem->localE)
    Vector_free(problem->localE);

  if (problem->capacitance_preconditioner)
    Preconditioner_free(problem->capacitance_preconditioner);
  
  free(problem);
}

void PCMproblem_initasym(PCMproblem problem, real soluteNetCharge, real alpha, real beta, real gamma) {
  problem->useasymmetric = 1;
  problem->localE = Vector_allocate(problem->numdielectricpanels[0]);
  problem->alpha = alpha;
  problem->beta = beta;
  problem->gamma = gamma;
  problem->mu = -problem->alpha * tanh(-problem->gamma);

  //  generateQualocationOperator(&(problem->qualoperator), NULL, 0, &(problem->diel->tree->panels), problem->numdielectricpanels, 1, NULL, 0, 0, NULL, problem->numdielectricpanels[0]);
}

void PCMproblem_getReactionPotentials(PCMproblem problem, Vector reactionpotentials, Vector3D *chargeLocations, unsigned int numcharges) {
  unsigned int i, j;
  real G;
  for (i = 0; i < numcharges; i++) {
    reactionpotentials[i] = 0.0;
    for (j = 0; j < problem->numdielectricpanels[0]; j++) {
      G = GreensFunction_oneoverr(problem->diel->tree->panels[j]->centroid, chargeLocations[i], NULL);
      reactionpotentials[i] += problem->surfaceCharges[j] * G; // area scaling is already incorporated into problem->surfaceCharges
    }
  }
}

void PCMproblem_solveSym(PCMproblem problem, Vector phiSurf) {
  Vector rhs = Vector_allocate(problem->numtotalsurfacevariables);
  PCMproblem_setupSternRHS(problem, phiSurf); // puts into problem->RHS
  problem->preconditioner = Preconditioner_allocate(problem->numtotalsurfacevariables, problem->numtotalsurfacevariables);
  Preconditioner_fill_identity(problem->preconditioner);
  Preconditioner_factor(problem->preconditioner);
  GMRES_PCMproblem(problem, problem->preconditioner, problem->RHS, problem->Sol, errortolerance); // puts into problem->Sol
  PCMproblem_findSurfaceCharges(problem); // finds surface charge values, see Matlab repo
  Vector_free(rhs);
}

void PCMproblem_solveAsym(PCMproblem problem, Vector phiSurf) {
}

void PCMproblem_setupSternRHS(PCMproblem problem, Vector phiSurf) {
  Vector_zero(problem->RHS, problem->numtotalsurfacevariables);
  Vector_copypiece(problem->RHS,0, phiSurf, 0, problem->numdielectricpanels[0]);
}


void PCMproblem_findSurfaceCharges(PCMproblem problem) {
  unsigned int i;
  Vector currentPhi = Vector_allocate(problem->numdielectricpanels[0]);
  Vector sigmaEff   = Vector_allocate(problem->numdielectricpanels[0]);
  Vector_copypiece(currentPhi, 0, problem->Sol, 0, problem->numdielectricpanels[0]);
  if (problem->capacitance_preconditioner == NULL) {
    problem->capacitance_preconditioner = Preconditioner_allocate(problem->numdielectricpanels[0],problem->numdielectricpanels[0]);
    Preconditioner_fill_diagonal_cap(problem->capacitance_preconditioner, problem->diel->tree);
    Preconditioner_factor(problem->capacitance_preconditioner);
  }
  GMRES_cap(problem->diel->tree, problem->capacitance_preconditioner, currentPhi, sigmaEff, errortolerance);
  for (i = 0; i < problem->numdielectricpanels[0]; i++) {
    problem->surfaceCharges[i] = sigmaEff[i] * problem->diel->tree->panels[i]->area;
  }
  Vector_free(sigmaEff);
  Vector_free(currentPhi);
}


void PCMproblem_writematlabfile(PCMproblem problem, char *filename) {
  unsigned int i, j;
  Vector x  = Vector_allocate(problem->numtotalsurfacevariables);
  Vector Ax = Vector_allocate(problem->numtotalsurfacevariables);

  FILE *file = NULL;
  file = fopen(filename,"w");
  for (i = 0; i < problem->numtotalsurfacevariables; i++) {
    Vector_zero(x,problem->numtotalsurfacevariables);
    x[i] = 1.0;
    PCMproblem_multiply(problem, Ax, x);
    for (j = 0; j < problem->numtotalsurfacevariables; j++) {
      fprintf(file, "%f  ",Ax[j]);
    }
    fprintf(file,"\n");
  }
  fclose(file);

  Vector_free(x);
  Vector_free(Ax);
}

void PCMproblem_multiply(PCMproblem problem, Vector Ax, Vector x) {
  Vector sternPhi  = Vector_allocate(problem->numsaltpanels[0]);
  Vector sternDPhi = Vector_allocate(problem->numsaltpanels[0]);
  Vector dielPhi   = Vector_allocate(problem->numdielectricpanels[0]);
  Vector dielDPhi   = Vector_allocate(problem->numdielectricpanels[0]);

  Vector AxRow1 = Vector_allocate(problem->numdielectricpanels[0]);
  Vector AxRow2 = Vector_allocate(problem->numdielectricpanels[0]);
  Vector AxRow3 = Vector_allocate(problem->numsaltpanels[0]);
  Vector AxRow4 = Vector_allocate(problem->numsaltpanels[0]);

  Vector A11x = Vector_allocate(problem->numdielectricpanels[0]);
  Vector A12x = Vector_allocate(problem->numdielectricpanels[0]);
  Vector A21x = Vector_allocate(problem->numdielectricpanels[0]);
  Vector A22x = Vector_allocate(problem->numdielectricpanels[0]);
  Vector A32x = Vector_allocate(problem->numsaltpanels[0]);
  Vector AbothPhi = Vector_allocate(problem->numsaltpanels[0]+problem->numdielectricpanels[0]);
  Vector AbothDPhi = Vector_allocate(problem->numsaltpanels[0]+problem->numdielectricpanels[0]);
  Vector bothPhi = Vector_allocate(problem->numsaltpanels[0]+problem->numdielectricpanels[0]);
  Vector bothDPhi = Vector_allocate(problem->numsaltpanels[0]+problem->numdielectricpanels[0]);
  Vector A43x = Vector_allocate(problem->numsaltpanels[0]);
  Vector A44x = Vector_allocate(problem->numsaltpanels[0]);
  
  // just to be safe, zero out the output vectors before starting

  Vector_copypiece(dielPhi, 0, x, 0, problem->numdielectricpanels[0]);
  Vector_copypiece(dielDPhi, 0, x, problem->numdielectricpanels[0], problem->numdielectricpanels[0]);
  Vector_copypiece(sternPhi, 0, x, 2*problem->numdielectricpanels[0], problem->numsaltpanels[0]);
  Vector_copypiece(sternDPhi, 0, x, 2*problem->numdielectricpanels[0]+problem->numsaltpanels[0], problem->numsaltpanels[0]);  
  
  Vector_zero(Ax, problem->numtotalsurfacevariables);
  Vector_zero(AxRow1, problem->numdielectricpanels[0]);
  Vector_zero(AxRow2, problem->numdielectricpanels[0]);
  Vector_zero(AxRow3, problem->numsaltpanels[0]);
  Vector_zero(AxRow4, problem->numsaltpanels[0]);

  Tree_multiply(A11x, problem->diel->tree, dielPhi, DOUBLE_LAYER_INT);
  Tree_multiply(A12x, problem->diel->tree, dielDPhi, SINGLE_LAYER_INT);

  Vector_copy(AxRow1, A11x, problem->numdielectricpanels[0]);
  Vector_addscaledvector(AxRow1, -1.0, A12x, problem->numdielectricpanels[0]);

  // sternPlusDiel Phi ROW 2 ... and 3
  Vector_copyscaledpiece(bothPhi, 0, sternPhi, 0, problem->numsaltpanels[0],1.0);
  Vector_copyscaledpiece(bothPhi, problem->numsaltpanels[0], dielPhi, 0, problem->numdielectricpanels[0],-1.0);
  Tree_multiply(AbothPhi, problem->sternPlusDiel->tree, bothPhi, DOUBLE_LAYER_INT);
  Vector_copyscaledpiece(A21x, 0, AbothPhi, problem->numsaltpanels[0], problem->numdielectricpanels[0], 1.0);
  Vector_addscaledvector(A21x, 4.0*M_PI, dielPhi, problem->numdielectricpanels[0]);
  Vector_copyscaledpiece(AxRow2, 0, A21x, 0, problem->numdielectricpanels[0],1.0);
  Vector_copypiece(AxRow3, 0, AbothPhi, 0, problem->numsaltpanels[0]);

  // sternPlusDiel DPhi ROW 3 ... and 2 
  Vector_copyscaledpiece(bothDPhi, 0, sternDPhi, 0, problem->numsaltpanels[0],-1.0);
  /* if (! problem->useasymmetric) { */
  Vector_scale(dielDPhi, innerdielectric/outerdielectric,problem->numdielectricpanels[0]);
  /* } else { */
  /*   printf("problem->useasymmetric = %d\n", problem->useasymmetric); */
  /*   unsigned int i; */
  /*   for (i = 0; i < problem->numdielectricpanels[0]; i++) { */
  /*     real f, h; */
  /*     h = problem->alpha * tanh(problem->beta*problem->localE[i] - problem->gamma) + problem->mu; */
  /*     f = innerdielectric/(outerdielectric-innerdielectric) - h; */
  /*     dielDPhi[i] = dielDPhi[i] * f/(1+f); */
  /*   } */
  /* } */
  Vector_copypiece(bothDPhi, problem->numsaltpanels[0], dielDPhi, 0, problem->numdielectricpanels[0]);
  Tree_multiply(AbothDPhi, problem->sternPlusDiel->tree, bothDPhi, SINGLE_LAYER_INT);
  Vector_copypiece(A22x,0,AbothDPhi,problem->numsaltpanels[0],problem->numdielectricpanels[0]);
  Vector_copypiece(A32x,0,AbothDPhi,0,problem->numsaltpanels[0]);
  Vector_addvector(AxRow2,A22x,problem->numdielectricpanels[0]);
  Vector_addvector(AxRow3,A32x,problem->numsaltpanels[0]);
  
  // ROW 4: good to go (I THINK)
  unsigned int i;
  real d1, d2;
  if (problem->useasymmetric) {
    if (fabs(problem->netcharge )> 1e-6) {
      d1 = 1.0;
      d2 = 1.0;
    } else {
      d1 = problem->netcharge;
      d2 = 0.0;
      for (i=0; i < problem->numsaltpanels[0]; i++) {
	d2 += problem->stern->tree->panels[i]->area * sternDPhi[i];
      }
    }
    
  } else {
    d1 = 1.0;
    d2 = 1.0;
  }    
  
  Tree_multiply(A43x, problem->stern->tree, sternPhi, DOUBLE_LAYER_INT);
  Vector_subtractscaledvector(A43x,4.0*M_PI,sternPhi,problem->numsaltpanels[0]);
  Vector_scale(sternDPhi, d1/d2, problem->numsaltpanels[0]);
  Tree_multiply(A44x, problem->stern->tree, sternDPhi, SINGLE_LAYER_INT);

  Vector_copyscaledpiece(AxRow4, 0, A43x, 0, problem->numsaltpanels[0], -1.0);
  Vector_addscaledvector(AxRow4, 1.0, A44x, problem->numsaltpanels[0]);

  // copy block row results into output vector
  Vector_copypiece(Ax, 0, AxRow1, 0, problem->numdielectricpanels[0]);
  Vector_copypiece(Ax, problem->numdielectricpanels[0], AxRow2, 0, problem->numdielectricpanels[0]);
  Vector_copypiece(Ax, 2*problem->numdielectricpanels[0], AxRow3, 0, problem->numsaltpanels[0]);
  Vector_copypiece(Ax, 2*problem->numdielectricpanels[0]+problem->numsaltpanels[0], AxRow4, 0, problem->numsaltpanels[0]);


  // free everything we allocated
  Vector_free(A11x);
  Vector_free(A12x);
  Vector_free(A21x);
  Vector_free(A22x);
  Vector_free(A32x);
  Vector_free(AbothPhi);
  Vector_free(AbothDPhi);
  Vector_free(bothPhi);
  Vector_free(bothDPhi);
  Vector_free(A43x);
  Vector_free(A44x);
  
  Vector_free(AxRow1);
  Vector_free(AxRow2);
  Vector_free(AxRow3);
  Vector_free(AxRow4);
  
  Vector_free(sternPhi);
  Vector_free(sternDPhi);
  Vector_free(dielPhi);
  Vector_free(dielDPhi);
}

