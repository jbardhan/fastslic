#ifndef __PCMPROBLEM_H__
#define __PCMPROBLEM_H__

#include "FFTSVDpbeAPI.h"

typedef struct _PCMproblem {
  unsigned int useasymmetric;
  Vector localE;
  Preconditioner      capacitance_preconditioner;
  real netcharge;
  real alpha;
  real beta;
  real gamma;
  real mu;
  
   unsigned int numsalts;
   Panel** saltpanels;
   unsigned int* numsaltpanels;

   unsigned int numdielectrics;
   Panel** dielectricpanels;
   unsigned int* numdielectricpanels;
   unsigned int* dielectricparent; 

   unsigned int numdielectriccavities;
   Panel** dielectriccavitypanels;
   unsigned int* numdielectriccavitypanels;
   unsigned int* dielectriccavityparent;

   unsigned int numsaltcavities;
   Panel** saltcavitypanels;
   unsigned int* numsaltcavitypanels;
   unsigned int* saltcavityparent;

   unsigned int numtotalpanels;
   unsigned int numtotalsurfacevariables; //  == 2 * numtotalpanels in green's thm,
   // but only 1 * numtotalpanels in qualocation
   
   /* Surface Operator Related */
   SurfaceOperator stern; 
   SurfaceOperator sternPlusDiel; 
   SurfaceOperator diel; 
   Preconditioner preconditioner;

   PDBentry* pdbentries;
   unsigned int numpdbentries;

   unsigned int numvariablecharges;
   unsigned int *variablechargeindextoglobalindex;
   unsigned int numfixedligandcharges;
   unsigned int *fixedligandchargeindextoglobalindex;
   unsigned int numfixedreceptorcharges;
   unsigned int *fixedreceptorchargeindextoglobalindex;

   Vector globalCharges;
   Vector RHS;
   Vector Sol;
   Vector surfaceCharges; // for PCM
   Vector globalPhiReact;
   Vector variablePhiReact;

   char *directory;  /* the bound and unbound PDB files are in
                        separate directories -- each uses the
                        appropriate delphi.prl fort.10, etc files */
} _PCMproblem;

typedef struct _PCMproblem* PCMproblem;

PCMproblem PCMproblem_allocate_fromSurfOper(SurfaceOperator stern, SurfaceOperator both, SurfaceOperator diel);
void PCMproblem_writematlabfile(PCMproblem problem, char *filename);
void PCMproblem_multiply(PCMproblem problem, Vector Ax, Vector x);
void PCMproblem_free(PCMproblem problem);
void PCMproblem_initasym(PCMproblem problem, real soluteNetCharge, real alpha, real beta, real gamma);
void PCMproblem_getReactionPotentials(PCMproblem problem, Vector reactionpotentials, Vector3D *chargeLocations, unsigned int numcharges);
void PCMproblem_solveSym(PCMproblem problem, Vector phiSurf);
void PCMproblem_solveAsym(PCMproblem problem, Vector phiSurf);
void PCMproblem_setupSternRHS(PCMproblem problem, Vector phiSurf);
void GMRES_PCMproblem(PCMproblem problem, Preconditioner preconditioner, Vector rhs, Vector sol, real tol);
void PCMproblem_findSurfaceCharges(PCMproblem problem);

/* PCMproblem PCMproblem_loadOnlyPDB(char *PDBfilename); */
/* void PCMproblem_allocateButDontCompress(PCMproblem problem, char *PDBfilename, char *SRFfilename); */
/* void PCMproblem_initialize(PCMproblem problem); */
/* void PCMproblem_loadNewChargeDistribution(PCMproblem problem, char *PDBfilename, char *CRGfilename); */
/* int PCMproblem_load(PCMproblem problem); */
/* void PCMproblem_setVariableChargeVector(PCMproblem problem, Vector variableCharges); */
/* void PCMproblem_getVariableReactionPotentials(PCMproblem problem, Vector variableReactPot); */
/* void PCMproblem_solve(PCMproblem problem); */
/* void PCMproblem_applyPreconditioner(PCMproblem problem, Vector Px, Vector x); */
/* void PCMproblem_applyA1(PCMproblem problem, Vector A1x, Vector x); */
/* void PCMproblem_applyA2(PCMproblem problem, Vector A2x, Vector x); */
/* void PCMproblem_applyA3(PCMproblem problem, Vector A3x, Vector x); */
/* void PCMproblem_findVariableChargeIndices(PCMproblem problem, char variablechain); */
/* void PCMproblem_findFixedLigandChargeIndices(PCMproblem problem, char fixedligandchain); */
/* void PCMproblem_findFixedReceptorChargeIndices(PCMproblem problem, char variablechain, char fixedligandchain); */

extern SIZentry* SIZentries;
extern unsigned int numSIZentries;
extern CRGentry* CRGentries;
extern unsigned int numCRGentries;
extern char variablechain;
extern char fixedligandchain;
extern char fixedreceptorchain;
   
#endif
